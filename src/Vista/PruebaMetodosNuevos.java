/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaCalificaciones;
import java.io.IOException;

/**
 *
 * @author madar
 */
public class PruebaMetodosNuevos {
    
    public static void main(String[] args) throws IOException {
        SistemaCalificaciones sistema=new SistemaCalificaciones("src/Datos/estudiantes.xls");
        
        //Se agradece una explicacion de cómo hacer para imprimir el metodo 1 y 2 usando el toString
        //Ya que actualmente sólo imprime recorriendo el arreglo.
        
        
        //Metodo 1
        Estudiante [] estudiantes = sistema.getReprobaronQuices();
        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println(estudiantes[i].toString());    
        }
        
        //Metodo 2
        /*Estudiante [] estudiantes = sistema.getMayorNotaQuices();
        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println(estudiantes[i].toString());    
        }*/
        
        //Metodo 3
        Float metodo3 = sistema.getNota_Que_MasRepite();
        System.out.println("NOTA QUE MÁS SE REPITE: " + metodo3);
        
        //Metodo 4
        String output = sistema.getNombreQuiz_Perdieron();
        System.out.println("EL QUIZ QUE MAS ESTUDIANTES PERDIERON: "+output);
        
       
        }
    }
    
